#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/systemd/Sanity/udev-rules
#   Description: Test for several udev rules
#   Author: Branislav Blaskovic <bblaskov@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="systemd"
RULES_DIR="/usr/lib/udev/rules.d"
RULES_REDHAT="$RULES_DIR/40-redhat.rules"

# Check if file pass as the first argument ($1) is a 'subfile' of the file
# from the second argument ($2)
function file_is_subfile_of() {
    local file1="${1:?Missing argument: first file}"
    local file2="${2:?Missing argument: second file}"
    local srcindex=0

    mapfile -t in1 < "$file1"
    mapfile -t in2 < "$file2"

    for ((i = 0; i < ${#in2[@]}; i++)); do
        # Single brackets are necessary, as [[ ]] would do unwanted bash
        # parameter expansion
        if [ "${in1[$srcindex]}" == "${in2[$i]}" ]; then
            srcindex=$((srcindex + 1))
            if [[ $srcindex == ${#in1[@]} ]]; then
                return 0
            fi
        else
            srcindex=0
        fi
    done

    return 1
}

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "cat $RULES_REDHAT"
    rlPhaseEnd

    rlPhaseStartTest "bug 1322773"
        rlAssertGrep 'SUBSYSTEM=="scsi", ENV{DEVTYPE}=="scsi_target", TEST!="\[module/sg\]", RUN+="/sbin/modprobe -bv sg"' $RULES_REDHAT
    rlPhaseEnd


if rlIsRHEL ">=7.4" || rlIsCentOS ">=7.4"; then
    rlPhaseStartTest "bug 1274401"
        rlAssertGrep 'KERNEL=="sd\*\[!0-9\]", SUBSYSTEMS=="scsi", ENV{.ID_ZFCP_BUS}=="1", ENV{DEVTYPE}=="disk", SYMLINK+="disk/by-path/ccw-$attr{hba_id}-zfcp-$attr{wwpn}:$attr{fcp_lun}"' $RULES_REDHAT
        rlAssertGrep 'KERNEL=="sd\*\[0-9\]", SUBSYSTEMS=="scsi", ENV{.ID_ZFCP_BUS}=="1", ENV{DEVTYPE}=="partition", SYMLINK+="disk/by-path/ccw-$attr{hba_id}-zfcp-$attr{wwpn}:$attr{fcp_lun}-part%n"' $RULES_REDHAT
        rlAssertGrep 'ACTION=="remove", GOTO="zfcp_scsi_device_end"' $RULES_REDHAT
        rlAssertGrep 'KERNEL=="sd\*", SUBSYSTEMS=="ccw", DRIVERS=="zfcp", ENV{.ID_ZFCP_BUS}="1"' $RULES_REDHAT
        rlAssertGrep 'LABEL="zfcp_scsi_device_end"' $RULES_REDHAT
    rlPhaseEnd
fi

if rlIsRHEL "<=7"; then
    rlPhaseStartTest "Dell: blacklist IR camera [BZ#1612896] (RHEL-7.6)"
        RULES_DELL="$RULES_DIR/40-redhat-disable-dell-ir-camera.rules"
        rlRun "test -f $RULES_DELL"
        rlAssertGrep 'SUBSYSTEM=="usb", ATTRS{idVendor}=="0BDA", ATTRS{idProduct}=="58F6", ATTR{authorized}="0"' $RULES_DELL
    rlPhaseEnd
fi

    rlPhaseStartTest "Implement new memory hotplug policy [BZ#1612896] (RHEL-7.6)"
        RULES_HOTPLUG="$(mktemp)"
        # Followup in #1666612 and #1713159 [RHEL-8 only, so far]
        # Introduction of CONST in #1748051 [RHEL-7] and #1762679 [RHEL-8]
        # \EOF prevents bash from expanding $variables
        cat > "$RULES_HOTPLUG" << \EOF
SUBSYSTEM!="memory", GOTO="memory_hotplug_end"
ACTION!="add", GOTO="memory_hotplug_end"
CONST{arch}=="s390*", GOTO="memory_hotplug_end"
EOF

        ! rlIsRHEL 7 && cat >> "$RULES_HOTPLUG" << \EOF
CONST{arch}=="ppc64*", GOTO="memory_hotplug_end"
EOF

        cat >> "$RULES_HOTPLUG" << \EOF

ENV{.state}="online"
CONST{virt}=="none", ENV{.state}="online_movable"
ATTR{state}=="offline", ATTR{state}="$env{.state}"

LABEL="memory_hotplug_end"
EOF
    rlRun "cat '$RULES_HOTPLUG'"
    rlRun "file_is_subfile_of '$RULES_HOTPLUG' '$RULES_REDHAT'"
    rlPhaseEnd

if rlIsRHEL 7; then
    rlPhaseStartTest "Add MODEL_ID for NVMe devices [BZ#1397264] (RHEL-7.6)"
        RULES_NVME="$(mktemp)"
        cat > "$RULES_NVME" << \EOF
KERNEL=="nvme*[0-9]n*[0-9]", ENV{DEVTYPE}=="disk", ATTRS{model}=="?*", ENV{ID_MODEL}="$attr{model}"
KERNEL=="nvme*[0-9]n*[0-9]", ENV{DEVTYPE}=="disk", ENV{ID_MODEL}=="?*", ENV{ID_SERIAL_SHORT}=="?*", ENV{ID_SERIAL}="$env{ID_MODEL}_$env{ID_SERIAL_SHORT}", SYMLINK+="disk/by-id/nvme-$env{ID_SERIAL}", OPTIONS="string_escape=replace"

KERNEL=="nvme*[0-9]n*[0-9]p*[0-9]", ENV{DEVTYPE}=="partition", ATTRS{serial}=="?*", ENV{ID_SERIAL_SHORT}="$attr{serial}"
KERNEL=="nvme*[0-9]n*[0-9]p*[0-9]", ENV{DEVTYPE}=="partition", ATTRS{model}=="?*", ENV{ID_MODEL}="$attr{model}"
KERNEL=="nvme*[0-9]n*[0-9]p*[0-9]", ENV{DEVTYPE}=="partition", ENV{ID_MODEL}=="?*", ENV{ID_SERIAL_SHORT}=="?*", ENV{ID_SERIAL}="$env{ID_MODEL}_$env{ID_SERIAL_SHORT}", SYMLINK+="disk/by-id/nvme-$env{ID_SERIAL}-part%n", OPTIONS="string_escape=replace"
EOF
        rlRun "cat '$RULES_NVME'"
        rlRun "file_is_subfile_of '$RULES_NVME' '$RULES_DIR/60-persistent-storage.rules'"
    rlPhaseEnd
fi

if ! rlIsRHEL || rlIsRHEL ">=8"; then
    rlPhaseStartTest "Watch metadata changes on DASD devices [BZ#1638676]"
        grep -E 'ACTION\!=\"remove\",\s*SUBSYSTEM==\"block\",\s*KERNEL==\".+\|dasd\*.*?\",\s*OPTIONS\+=\"watch\"' <(sed -e 's/ \\$//g' $RULES_DIR/60-block.rules | tr -d '\n')
        rlAssert0 "dasd devices are included in the watch udev rule" $?
    rlPhaseEnd
fi

if ! rlIsRHEL || rlIsRHEL ">=8"; then
    rlPhaseStartTest "Add elevator= kernel command line parameter [BZ#1670126]"
        IFS="" read -r -d '' ELEVATOR_RULE << \EOF
# We aren't adding devices skip the elevator check
ACTION!="add", GOTO="sched_out"

SUBSYSTEM!="block", GOTO="sched_out"
ENV{DEVTYPE}!="disk", GOTO="sched_out"

# Technically, dm-multipath can be configured to use an I/O scheduler.
# However, there are races between the 'add' uevent and the linking in
# of the queue/scheduler sysfs file.  For now, just skip dm- devices.
KERNEL=="dm-*|md*", GOTO="sched_out"

# Skip bio-based devices, which don't support an I/O scheduler.
ATTR{queue/scheduler}=="none", GOTO="sched_out"

# If elevator= is specified on the kernel command line, change the
# scheduler to the one specified.
IMPORT{cmdline}="elevator"
ENV{elevator}!="", ATTR{queue/scheduler}="$env{elevator}"

LABEL="sched_out"
EOF
        # For $'x' syntax check the ANSI-C section of the Bash Reference Manual
        # The expression below basically strips the trailing newline from the
        # heredoc string above
        ELEVATOR_RULE="${ELEVATOR_RULE%$'\n'}"
        echo "$ELEVATOR_RULE"
        rlRun "test -f $RULES_DIR/40-elevator.rules"
        diff "$RULES_DIR/40-elevator.rules" <(echo -n "$ELEVATOR_RULE")
        rlAssert0 "Check if the content of $RULES_DIR/40-elevator.rules differs from the expected one" $?
    rlPhaseEnd
fi

if ! rlIsRHEL || rlIsRHEL ">=8"; then
    rlPhaseStartTest "Support WAIT_FOR key in udev rules [BZ#1523213]"
        TEST_RULE="$(mktemp /etc/udev/rules.d/99-testXXX.rules)"
        TEST_DEVICE="/dev/input/event0"
        JOURNAL_TS="$(date +"%Y-%m-%d %H:%M:%S")"
        JOURNAL_LOG="$(mktemp journal-WAIT_FOR-dump-XXX.log)"

        cat > "$TEST_RULE" << EOF
WAIT_FOR="$TEST_DEVICE"
EOF
        rlRun "cat '$TEST_RULE'"

        rlRun "udevadm control --log-priority debug"
        rlRun "udevadm control --reload"
        rlRun "udevadm trigger --settle"
        rlRun "journalctl --since '$JOURNAL_TS' > '$JOURNAL_LOG'"

        if rlIsRHEL "<9"; then
            # file '/dev/input/event0' appeared after 0 loops
            grep -E "file '$TEST_DEVICE' appeared after [0-9]+ loops" "$JOURNAL_LOG"
            rlAssert0 "journal should contain: file '$TEST_DEVICE' appeared after [0-9]+ loops" $?
        fi
        rlAssertNotGrep "(Invalid|unknown) key 'WAIT_FOR'" "$JOURNAL_LOG" -Ei

        rlFileSubmit "$JOURNAL_LOG"

        rlRun "udevadm control --log-priority info"
        rlRun "udevadm control --reload"
        rlRun "udevadm trigger --settle"
        rlRun "rm -f $TEST_RULE"
    rlPhaseEnd
fi

if rlIsRHEL "<=8"; then
    rlPhaseStartTest "udev.alias= [BZ#1739353]"
        RULES_UDEV_ALIAS="$(mktemp)"

        cat > "$RULES_UDEV_ALIAS" << \EOF
SUBSYSTEM!="block", GOTO="log_end"
KERNEL=="loop*|ram*", GOTO="log_end"
ACTION=="remove", GOTO="log_end"
ENV{DM_UDEV_DISABLE_OTHER_RULES_FLAG}=="1", GOTO="log_end"
ENV{DM_UDEV_DISABLE_DISK_RULES_FLAG}=="1", GOTO="log_end"

IMPORT{cmdline}="udev.alias"
ENV{udev.alias}=="1", RUN+="/bin/sh -c 'echo udev-alias: $name \($links\) > /dev/kmsg'"

LABEL="log_end"
EOF

        rlRun "cat '$RULES_UDEV_ALIAS'"
        rlRun "test -f '$RULES_DIR/60-alias-kmsg.rules'"
        rlRun "diff '$RULES_UDEV_ALIAS' '$RULES_DIR/60-alias-kmsg.rules'"

        rlRun "rm -fv '$RULES_UDEV_ALIAS'"
    rlPhaseEnd
fi

if ! rlIsRHEL || rlIsRHEL ">=7.8" || rlIsRHEL ">=8.2"; then
    rlPhaseStartTest "udev-rules: make tape-changers also appear in /dev/tape/by-path/ [BZ#1820112]"
        RULES_VERITAS="$(mktemp)"

        cat > "$RULES_VERITAS" << \EOF
SUBSYSTEM=="scsi_generic", SUBSYSTEMS=="scsi", ATTRS{type}=="8", IMPORT{builtin}="path_id", \
  SYMLINK+="tape/by-path/$env{ID_PATH}-changer"
EOF

        rlRun "cat '$RULES_VERITAS'"
        rlRun "file_is_subfile_of '$RULES_VERITAS' '$RULES_DIR/60-persistent-storage-tape.rules'"
    rlPhaseEnd
fi

    rlPhaseStartCleanup
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

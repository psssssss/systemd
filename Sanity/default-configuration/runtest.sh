#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/systemd/Sanity/default-configuration
#   Description: Check the default systemd configuration
#   Author: Frantisek Sumsal <fsumsal@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="systemd"

set -o pipefail

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest "Set RemoveIPC=no by default [BZ#1523233]"
        rlRun "loginctl show-session -p RemoveIPC"
        if rlIsRHEL 7; then
            # On RHEL 7 RemoveIPC may not be set and thus it's not shown in
            # the show-session output
            LOGINCT_SHOW="$(loginctl show-session -p RemoveIPC | awk -F= '{print $2}')"
            if [[ $LOGINCT_SHOW ]]; then
                rlRun "[[ $LOGINCT_SHOW == 'no' ]]"
            fi
        else
            rlRun "[[ $(loginctl show-session -p RemoveIPC --value) == 'no' ]]"
        fi
        rlRun "grep RemoveIPC=no /etc/systemd/logind.conf"
    rlPhaseEnd

if rlIsRHEL ">=8" || rlIsCentOS ">=8"; then
    rlPhaseStartTest "Set TasksMax=80% by default [BZ#1523236]"
        # System sets DefaultTasksLimit to a static value during initrams, which
        # is, most probably, derived from an incorrect value of kernel.pid_max
        # (or DefaultTasksMax), thus the expected runtime value differs from the
        # actually used one. Needs further investigation.
        #KERNEL_PID_MAX="$(sysctl --values kernel.pid_max)"
        ## TasksMax is set to 80% of available PIDs
        #EIGHTY_PERCENT=$((KERNEL_PID_MAX / 5 * 4))

        # Fedora's default is 4915, let's set the static limit to 5k until
        # the dynamic limit above is fixed
        TASKS_LIMIT=5000

        rlRun "systemctl show -p DefaultTasksMax"
        rlRun "[[ $(systemctl show -p DefaultTasksMax --value) -gt $TASKS_LIMIT ]]"
        rlRun "grep 'DefaultTasksMax=80%' /etc/systemd/system.conf"

        rlRun "systemctl show systemd-journald.service -p TasksMax"
        rlRun "[[ $(systemctl show systemd-journald.service -p TasksMax --value) -gt $TASKS_LIMIT ]]"
    rlPhaseEnd

    rlPhaseStartTest "Set kernel.pid_max to 2^22 [BZ#1744214]"
        REPORTED=$(sysctl --values kernel.pid_max)
        EXPECTED=$((2**22))

        rlLogInfo "Reported kernel.pid_max: $REPORTED"
        rlLogInfo "Expected kernel.pid_max: $EXPECTED"
        rlRun "[[ $REPORTED -eq $EXPECTED ]]"
    rlPhaseEnd

    rlPhaseStartTest "Check default RLIMITs after daemon-reload [BZ#1789930]"
        function check_rlimits() {
            # Values taken from src/basic/def.h
            if rlIsRHEL "<=8"; then
                local -i expected_NOFILE=$((256 * 1024))
            else
                local -i expected_NOFILE=$((512 * 1024))
            fi
            # MEMLOCK is defined in bytes, but systemctl returns it in KBytes
            local -i expected_MEMLOCK=$((1024 * 64))
            local reported expected

            for limit in NOFILE MEMLOCK; do
                varname="expected_$limit"
                # Indirect expansion
                expected=${!varname}

                rlLogInfo "Expected $limit: $expected"
                reported=$(systemctl show --value -p DefaultLimit$limit)
                rlLogInfo "Reported $limit: $reported"
                rlRun "[[ $reported -eq $expected ]]"
            done
        }

        rlLogInfo "Before reload"
        check_rlimits

        rlRun "systemctl daemon-reload"
        rlLogInfo "After reload"
        check_rlimits

        rlRun "systemctl daemon-reexec"
        rlLogInfo "After reexec"
        check_rlimits
    rlPhaseEnd
fi

if rlIsRHEL ">=8" || rlIsCentOS ">=8"; then
    rlPhaseStartTest "rc-local: order after network-online.target [BZ#1934028]"
        rlRun "[[ '$(systemctl show -p After --value rc-local.service)' =~ network-online\.target ]]"
        rlRun "[[ '$(systemctl show -p Wants --value rc-local.service)' =~ network-online\.target ]]"
    rlPhaseEnd
fi

    rlPhaseStartTest "Don't use BFQ as the default I/O scheduler [BZ#1936967]"
        while read -r blk; do
            rlLogInfo "Checking block device $blk"
            if [[ -e /sys/block/$blk/queue/scheduler ]]; then
                rlLogInfo "$blk: $(cat "/sys/block/$blk/queue/scheduler")"
                rlAssertNotGrep "[bfq]" "/sys/block/$blk/queue/scheduler" -Fi
            fi
        done < <(ls /sys/block/)
    rlPhaseEnd

if rlIsRHEL ">7" || rlIsCentOS ">7"; then
    rlPhaseStartTest "Don't ship /etc/resolv.conf symlink with resolved [BZ#1989472]"
        # Just a sanity check that --cat-config works as expected
        rlRun "systemd-tmpfiles --cat-config | grep -- /var/log/journal"
        rlRun "systemd-tmpfiles --cat-config | grep -- /etc/resolv.conf" 1
    rlPhaseEnd
fi

    rlPhaseStartTest "Default core ulimit should be 0, but should be overridable by user [BZ#1905582]"
        TESTUSER="testcore$RANDOM"
        rlRun "useradd $TESTUSER"

        rlRun "[[ '$(ulimit -c)' == '0' ]]" 0 "Check the core ulimit as root"
        rlRun "[[ '$(sudo -u $TESTUSER -- bash -c 'ulimit -c')' == '0' ]]" 0 "Check the core ulimit as an unprivileged user"
        rlRun "[[ '$(sudo -u $TESTUSER -- bash -c 'ulimit -c unlimited && ulimit -c')' == 'unlimited' ]]" 0 "Attempt to change the core ulimit as an unprivileged user"

        rlRun "userdel -rf $TESTUSER"
    rlPhaseEnd

if rlIsRHEL ">=8" || rlIsCentOS ">=8"; then
    rlPhaseStartTest "Make sure we have the correct MACAddressPolicy= [BZ#1921094]"
        if rlIsRHEL 8; then
            EXPECTED_POLICY="persistent"
        else
            EXPECTED_POLICY="none"
            # Change introduced by BZ#1921094
        fi

        rlAssertGrep "MACAddressPolicy=$EXPECTED_POLICY" /usr/lib/systemd/network/99-default.link
        if grep -v -F "MACAddressPolicy=$EXPECTED_POLICY" /usr/lib/systemd/network/99-default.link | grep "MACAddressPolicy="; then
            rlFail "/usr/lib/systemd/network/99-default.link contains an unexpected MACAddressPolicy= directive"
        fi
    rlPhaseEnd
fi

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

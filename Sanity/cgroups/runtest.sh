#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/systemd/Sanity/cgroups
#   Description: Check functionality of various cgroup-related settings/utils
#   Author: Frantisek Sumsal <fsumsal@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="systemd"
SYSTEMD_PAGER=

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlImport systemd/basic
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

if rlIsRHEL 7; then
    rlPhaseStartTest "Ensure we use the legacy cgroup hierarchy"
        # If this returns "cgroups2fs" we're in full cgroupsv2 mode,
        # otherwise it should return tmpfs (for hybrid and legacy modes)
        rlRun "[[ $(basic_get_cgroup_hierarchy) == 'legacy' ]]"
    rlPhaseEnd
fi

if [[ "$(basic_get_cgroup_hierarchy)" == "legacy" ]]; then
    rlPhaseStartTest "_CGROUP_CONTROLLER_MASK_ALL does not cover CGROUP_PIDS [BZ#1532586]"
        UNIT_PATH="$(mktemp /etc/systemd/system/cgroupsXXX.service)"
        UNIT_NAME="${UNIT_PATH##*/}"

        # Setup
        cat > "$UNIT_PATH" << EOF
[Service]
ExecStart=/bin/sleep 99h
Delegate=yes
EOF
        rlRun "systemctl daemon-reload"
        rlRun "systemctl cat $UNIT_NAME"
        rlRun "systemctl start $UNIT_NAME"

        # Test
        rlRun "eval $(systemctl show -p MainPID $UNIT_NAME)"
        rlRun "[[ ! -z $MainPID ]]"
        rlRun "grep -E 'pids:.*$UNIT_NAME' /proc/$MainPID/cgroup"

        # Cleanup
        rlRun "systemctl stop $UNIT_NAME"
        rlRun "rm -fv $UNIT_PATH"
        rlRun "systemctl daemon-reload"
    rlPhaseEnd

    if ! rlIsRHEL 7; then
        rlPhaseStartTest "FreezerState is incorrectly updated on system running cgroups v1 [BZ#1868831]"
            UNIT_NAME="freezerstate$RANDOM.service"

            rlRun "systemd-run --unit '$UNIT_NAME' /bin/sleep infinity"
            rlRun "systemctl restart '$UNIT_NAME'"
            FREEZER_STATE="$(systemctl show --value -p FreezerState "$UNIT_NAME")"
            rlLogInfo "Expected state: running"
            rlLogInfo "Current state: $FREEZER_STATE"
            rlRun "[[ '$FREEZER_STATE' == running ]]"

            # Cleanup
            rlRun "systemctl stop '$UNIT_NAME'" 0-255
        rlPhaseEnd
    fi
fi

if ! rlIsRHEL 7 && ! rlIsRHEL "<8.2"; then
    rlPhaseStartTest "AllowedCPUs="
        # Arguments:
        #   $1 - value for AllowedCPUs=
        #   $2 - expected value of EffectiveCPUs=
        # Returns:
        #   0 on success (service correctly starts up and has an expected value
        #   in EffectiveCPUs=), 1 otherwise
        # Note:
        #   wrap the whole function body in a subshell - () - so we don't propagate
        #   set -e outside of the function
        test_allowedcpus() {(
            set -e

            if [[ $# -ne 2 ]]; then
                echo >&2 "[${FUNCNAME[0]}] Invalid arguments"
                return 1
            fi

            local UNIT_PATH="$(mktemp /etc/systemd/system/allowedcpusXXX.service)"
            local UNIT_NAME="${UNIT_PATH##*/}"
            local TRANSIENT_NAME="allowedcpus$RANDOM.service"
            local VALUE

            # Register a cleanup handler
            trap "set +e; systemctl -q stop $UNIT_NAME $TRANSIENT_NAME; rm $UNIT_PATH; systemctl daemon-reload" EXIT

            echo "[${FUNCNAME[0]}] AllowedCPUs=$1 EffectiveCPUs=$2"

            echo "--- Check 1: standard service ---"
            printf "[Service]\nAllowedCPUs=$1\nExecStart=/bin/sleep infinity" > "$UNIT_PATH"
            systemctl daemon-reload
            systemctl cat "$UNIT_NAME"
            systemctl start "$UNIT_NAME"
            sleep 1
            systemctl status "$UNIT_NAME"
            VALUE="$(systemctl show --value -p EffectiveCPUs "$UNIT_NAME")"
            echo -e "Got: $VALUE\nExpected: $2"
            [[ "$VALUE" == "$2" ]]

            echo "--- Check 2: transient service ---"
            systemd-run --unit "$TRANSIENT_NAME" -p AllowedCPUs=$1 /bin/sleep infinity
            sleep 1
            systemctl cat "$TRANSIENT_NAME"
            systemctl status "$TRANSIENT_NAME"
            VALUE="$(systemctl show --value -p EffectiveCPUs "$TRANSIENT_NAME")"
            echo -e "Got: $VALUE\nExpected: $2"
            [[ "$VALUE" == "$2" ]]
        )}

        if ! NPROC=$(nproc); then
            rlFail "Couldn't determine # of CPUs"
        else
            # Check if we're running on a unified cgroup hierarchy (cgroup v2)
            # or legacy/hybrid hierarchy
            if [[ "$(basic_get_cgroup_hierarchy)" == "unified" ]]; then
                rlLogInfo "Detected UNIFIED cgroup hierarchy"


                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 1 1"
                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 1,1,1,1,1,1,1,1 1"
                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 1-1 1"
                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 0,1 0-1"
                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 0-1 0-1"
                # This is also a valid assignment
                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 0,,,,,,1 '0-1'"

                [[ $NPROC -ge 4 ]] && rlRun "test_allowedcpus 0,2-3 '0 2-3'"
                [[ $NPROC -ge 4 ]] && rlRun "test_allowedcpus 0,2 '0 2'"
                [[ $NPROC -ge 4 ]] && rlRun "test_allowedcpus 0-1,2-3 0-3"
                [[ $NPROC -ge 4 ]] && rlRun "test_allowedcpus 0,1,2,3 0-3"
                # This is also a valid assignment
                [[ $NPROC -ge 4 ]] && rlRun "test_allowedcpus 0,,,,,,3 '0 3'"

                [[ $NPROC -ge 8 ]] && rlRun "test_allowedcpus 1-3,4-6 1-6"
                [[ $NPROC -ge 8 ]] && rlRun "test_allowedcpus 0,2,4,6, '0 2 4 6'"
                [[ $NPROC -ge 8 ]] && rlRun "test_allowedcpus 1-3,5-7 '1-3 5-7'"
            elif [[ "$(basic_get_cgroup_hierarchy)" == "legacy" ]]; then
                # On legacy/hybrid cgroup hierarchy the EffectiveCPUs= property
                # should remain unset no matter what value AllowedCPUs= has
                rlLogInfo "Detected LEGACY/HYBRID  cgroup hierarchy"

                rlRun "test_allowedcpus 0 ''"

                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 1 ''"
                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 1,1,1,1,1,1,1,1 ''"
                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 1-1 ''"
                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 0,1 ''"
                [[ $NPROC -ge 2 ]] && rlRun "test_allowedcpus 0-1 ''"

                [[ $NPROC -ge 4 ]] && rlRun "test_allowedcpus 0,2-3 ''"
                [[ $NPROC -ge 4 ]] && rlRun "test_allowedcpus 0,2 ''"
                [[ $NPROC -ge 4 ]] && rlRun "test_allowedcpus 0-1,2-3 ''"
                [[ $NPROC -ge 4 ]] && rlRun "test_allowedcpus 0,1,2,3 ''"

                [[ $NPROC -ge 8 ]] && rlRun "test_allowedcpus 1-3,4-6 ''"
                [[ $NPROC -ge 8 ]] && rlRun "test_allowedcpus 0,2,4,6, ''"
                [[ $NPROC -ge 8 ]] && rlRun "test_allowedcpus 1-3,5-7 ''"
            else
                rlFail "Unsupported cgroup hierarchy: $(basic_get_cgroup_hierarchy)"
            fi

            # Following assignments should fail no matter what hierarchy we're
            # running on
            rlRun "test_allowedcpus 1--3 ''" 1
            rlRun "test_allowedcpus -1 ''" 1
            rlRun "test_allowedcpus 1- ''" 1
        fi
    rlPhaseEnd
fi

if ! rlIsRHEL 7 && [[ "$(basic_get_cgroup_hierarchy)" == "unified" ]] && [[ ! $(ostree admin status) ]]; then
    rlPhaseStartTest "IODeviceLatencyTargetSec="
        LATENCY="32"
        # We need to use a valid (existing) block device, so let's use the rootfs
        # one as it should always exist
        ROOTFS_DEV="$(findmnt -n -o SOURCE /)"
        # The io.latency controller saves the used block device as a major:minor
        # number pair, where the minor number is always 0 (the "root" of the device)
        ROOTFS_DEV_MAJ="$(lsblk -n -o MAJ:MIN "$ROOTFS_DEV" | awk -F: '{print $1}')"
        TRANSIENT_NAME="iodevicelatencytransient$RANDOM.service"
        UNIT_PATH="$(mktemp /etc/systemd/system/iodevicelatencystaticXXX.service)"
        UNIT_NAME="${UNIT_PATH##*/}"

        if [[ -z "$ROOTFS_DEV" || -z "$ROOTFS_DEV_MAJ" ]]; then
            rlDie "Failed to get the rootfs device"
        fi

        rlLogInfo "Using block device $ROOTFS_DEV with major number $ROOTFS_DEV_MAJ"

        rlLogInfo "Transient unit"
        rlRun "systemd-run --unit $TRANSIENT_NAME -p 'IODeviceLatencyTargetSec=$ROOTFS_DEV ${LATENCY}ms' /bin/sleep infinity"
        rlRun "systemctl status $TRANSIENT_NAME"
        rlLogInfo "Check if 'systemctl show' returns a correct value"
        rlRun "[[ '$(systemctl show --value -p IODeviceLatencyTargetUSec $TRANSIENT_NAME)' == '$ROOTFS_DEV ${LATENCY}ms' ]]"
        rlLogInfo "Check if the io.latency controller has a correct value"
        rlRun "[[ '$(</sys/fs/cgroup/system.slice/$TRANSIENT_NAME/io.latency)' == '$ROOTFS_DEV_MAJ:0 target=${LATENCY}000' ]]"
        rlRun "systemctl stop $TRANSIENT_NAME"

        rlLogInfo "Static unit"
        cat >> "$UNIT_PATH" << EOF
[Service]
ExecStart=/bin/sleep infinity
IODeviceLatencyTargetSec=$ROOTFS_DEV ${LATENCY}ms
EOF
        rlRun "systemctl daemon-reload"
        rlRun "systemctl cat $UNIT_NAME"
        rlRun "systemctl start $UNIT_NAME"
        rlRun "systemctl status $UNIT_NAME"
        rlLogInfo "Check if 'systemctl show' returns a correct value"
        rlRun "[[ '$(systemctl show --value -p IODeviceLatencyTargetUSec $UNIT_NAME)' == '$ROOTFS_DEV ${LATENCY}ms' ]]"
        rlLogInfo "Check if the io.latency controller has a correct value"
        rlRun "[[ '$(</sys/fs/cgroup/system.slice/$UNIT_NAME/io.latency)' == '$ROOTFS_DEV_MAJ:0 target=${LATENCY}000' ]]"
        rlRun "systemctl stop $UNIT_NAME"

        # Cleanup
        rlRun "rm -fv '$UNIT_PATH'"
        rlRun "systemctl daemon-reload"
    rlPhaseEnd

    rlPhaseStartTest "MemoryMin= & MemoryLow="
        MEMORY_MIN_MB="64"
        MEMORY_MIN_B="$((MEMORY_MIN_MB * 1024 * 1024))"
        MEMORY_LOW_MB="128"
        MEMORY_LOW_B="$((MEMORY_LOW_MB * 1024 * 1024))"
        TRANSIENT_NAME="memoryminlowtransient$RANDOM.service"
        UNIT_PATH="$(mktemp /etc/systemd/system/memoryminlowstaticXXX.service)"
        UNIT_NAME="${UNIT_PATH##*/}"

        rlLogInfo "Defined limits:"
        rlLogInfo "MemoryMin=${MEMORY_MIN_MB}M ($MEMORY_MIN_B bytes)"
        rlLogInfo "MemoryLow=${MEMORY_LOW_MB}M ($MEMORY_LOW_B bytes)"

        rlLogInfo "Transient unit"
        rlRun "systemd-run --unit $TRANSIENT_NAME -p MemoryMin=${MEMORY_MIN_MB}M -p MemoryLow=${MEMORY_LOW_MB}M /bin/sleep infinity"
        rlRun "systemctl status $TRANSIENT_NAME"
        rlLogInfo "Check if 'systemctl show' returns correct values"
        rlRun "[[ '$(systemctl show --value -p MemoryMin $TRANSIENT_NAME)' -eq '$MEMORY_MIN_B' ]]"
        rlRun "[[ '$(systemctl show --value -p MemoryLow $TRANSIENT_NAME)' -eq '$MEMORY_LOW_B' ]]"
        rlLogInfo "Check if the memory.min and memory.low controllers have correct values"
        rlRun "[[ '$(</sys/fs/cgroup/system.slice/$TRANSIENT_NAME/memory.min)' -eq '$MEMORY_MIN_B' ]]"
        rlRun "[[ '$(</sys/fs/cgroup/system.slice/$TRANSIENT_NAME/memory.low)' -eq '$MEMORY_LOW_B' ]]"
        rlRun "systemctl stop $TRANSIENT_NAME"

        rlLogInfo "Static unit"
        cat >> "$UNIT_PATH" <<EOF
[Service]
ExecStart=/bin/sleep infinity
MemoryMin=${MEMORY_MIN_MB}M
MemoryLow=${MEMORY_LOW_MB}M
EOF
        rlRun "systemctl daemon-reload"
        rlRun "systemctl cat $UNIT_NAME"
        rlRun "systemctl start $UNIT_NAME"
        rlRun "systemctl status $UNIT_NAME"
        rlLogInfo "Check if 'systemctl show' returns correct values"
        rlRun "[[ '$(systemctl show --value -p MemoryMin $UNIT_NAME)' -eq '$MEMORY_MIN_B' ]]"
        rlRun "[[ '$(systemctl show --value -p MemoryLow $UNIT_NAME)' -eq '$MEMORY_LOW_B' ]]"
        rlLogInfo "Check if the memory.min and memory.low controllers have correct values"
        rlRun "[[ '$(</sys/fs/cgroup/system.slice/$UNIT_NAME/memory.min)' -eq '$MEMORY_MIN_B' ]]"
        rlRun "[[ '$(</sys/fs/cgroup/system.slice/$UNIT_NAME/memory.low)' -eq '$MEMORY_LOW_B' ]]"
        rlRun "systemctl stop $UNIT_NAME"

        # Cleanup
        rlRun "rm -fv '$UNIT_PATH'"
        rlRun "systemctl daemon-reload"
    rlPhaseEnd

    rlPhaseStartTest "IOWeight="
        TRANSIENT_NAME="ioweighttransient$RANDOM.service"
        UNIT_PATH="$(mktemp /etc/systemd/system/ioweightstaticXXX.service)"
        UNIT_NAME="${UNIT_PATH##*/}"

        rlLogInfo "Transient unit"
        rlRun "systemd-run --unit $TRANSIENT_NAME -p IOWeight=666 /bin/sleep infinity"
        rlRun "systemctl status $TRANSIENT_NAME"
        rlLogInfo "Check if 'systemctl show' returns correct values"
        rlRun "[[ '$(systemctl show --value -p IOWeight $TRANSIENT_NAME)' -eq 666 ]]"
        rlLogInfo "Check if the io.bfq.weight controller has a correct value"
        if rlIsRHEL 8; then
            rlRun "[[ '$(awk '{print $2;}' /sys/fs/cgroup/system.slice/$TRANSIENT_NAME/io.bfq.weight)' -eq 666 ]]"
        else
            # https://github.com/systemd/systemd/issues/22897#issuecomment-1081809706
            rlRun "[[ '$(awk '{print $2;}' /sys/fs/cgroup/system.slice/$TRANSIENT_NAME/io.bfq.weight)' -eq 151 ]]"
        fi
        rlRun "systemctl stop $TRANSIENT_NAME"

        rlLogInfo "Static unit"
        cat >> "$UNIT_PATH" <<EOF
[Service]
ExecStart=/bin/sleep infinity
IOWeight=666
EOF
        rlRun "systemctl daemon-reload"
        rlRun "systemctl cat $UNIT_NAME"
        rlRun "systemctl start $UNIT_NAME"
        rlRun "systemctl status $UNIT_NAME"
        rlLogInfo "Check if 'systemctl show' returns correct values"
        rlRun "[[ '$(systemctl show --value -p IOWeight $UNIT_NAME)' -eq 666 ]]"
        rlLogInfo "Check if the io.bfq.weight controller has a correct value"
        if rlIsRHEL 8; then
            rlRun "[[ '$(awk '{print $2;}' /sys/fs/cgroup/system.slice/$UNIT_NAME/io.bfq.weight)' -eq 666 ]]"
        else
            # https://github.com/systemd/systemd/issues/22897#issuecomment-1081809706
            rlRun "[[ '$(awk '{print $2;}' /sys/fs/cgroup/system.slice/$UNIT_NAME/io.bfq.weight)' -eq 151 ]]"
        fi
        rlRun "systemctl stop $UNIT_NAME"

        # Cleanup
        rlRun "rm -fv '$UNIT_PATH'"
        rlRun "systemctl daemon-reload"
    rlPhaseEnd
fi

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

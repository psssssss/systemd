#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/systemd/Sanity/remote-service-management
#   Description: remote service management
#   Author: Petr Sklenar <psklenar@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="systemd"

rlJournalStart
    rlPhaseStartSetup
        rlImport systemd/basic
        rlAssertRpm $PACKAGE
        rlFileBackup --clean "/root/.ssh/" "/etc/ssh/ssh_config"
        username=petr$RANDOM
        conf=ssh_config

        echo 'Host localhost' >> /etc/ssh/ssh_config
        echo ' UserKnownHostsFile      /dev/null'  >> /etc/ssh/ssh_config
        echo ' StrictHostKeyChecking   no'  >> /etc/ssh/ssh_config
        echo ' HostbasedAuthentication no'  >> /etc/ssh/ssh_config
    rlPhaseEnd

    rlPhaseStartTest
        basicWaitForBootup
# copy&paste from http://pkgs.devel.redhat.com/cgit/tests/openssh/tree/sshd/sanity/runtest.sh
        rlRun "useradd $username"
        rlRun "echo $username | passwd --stdin $username"
        homedir=`getent passwd $username | awk -F: '{print $6}'`
        ls /root/.ssh/id_rsa /root/.ssh/id_rsa.pub || yes | ssh-keygen -t rsa -N  '' -f /root/.ssh/id_rsa
        rlAssert0 "ssh-keygen passed" $?
        mkdir /home/$username/.ssh
        cat /root/.ssh/id_rsa.pub >> /home/$username/.ssh/authorized_keys
        cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
        restorecon -R /home/$username/.ssh
    rlPhaseEnd


    rlPhaseStartTest
        for i in `seq 1 100`; do
            rlRun "systemctl --host $username@localhost list-unit-files > logs.${i}"
            lines=$(cat logs.${i} | grep enable | wc -l)
            if [[ "$lines" -ge "20" ]];then
                rlPass "there are $lines enabled services"
            else
                rlFail "there are $lines enabled services, its few"
            fi
        done
    rlPhaseEnd

    rlPhaseStartCleanup
        userdel -f -r $username
        rlLog "clean up"
        rlFileRestore

    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/systemd/Sanity/check-file-permissions
#   Description: Checks permissions of various system files
#   Author: Frantisek Sumsal <fsumsal@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="systemd"
# 0666 /dev/fuse
DEV_FUSE_PERMS="666"
# system_u:object_r:systemd_logind_var_run_t:s0 /var/run/nologin
VAR_RUN_NOLOGIN_CONTEXT="system_u:object_r:systemd_logind_var_run_t:s0"
# 0644 /dev/prandom
DEV_PRANDOM_PERMS="644"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest "BZ#1213972 - /dev/fuse permissions"
        rlRun "[[ \"$DEV_FUSE_PERMS\" == \"$(stat -c '%a' /dev/fuse)\" ]]" 0 "/dev/fuse should have 0666"
        rlLogInfo "$(stat -c '%a %n' /dev/fuse)"
    rlPhaseEnd

    rlPhaseStartTest "BZ#1264073 - /var/run/nologin context"
        rlRun "shutdown -r 4" 0 "Start shutdown counter - systemd should create /var/run/nologin"
        rlRun "sleep 5"
        rlRun "[[ \"$VAR_RUN_NOLOGIN_CONTEXT\" == \"$(stat -c '%C' /var/run/nologin)\" ]]" 0 \
              "/var/run/nologin should be created with system_u:object_r:systemd_logind_var_run_t:s0 context"
        rlLogInfo "$(stat -c '%C %n' /var/run/nologin)"
        rlRun "shutdown -c" 0 "Cancel shutdown"
    rlPhaseEnd

    rlPhaseStartTest "BZ#1264112 - /dev/prandom permissions (s390x only)"
        if [[ "$(rlGetPrimaryArch)" == "s390x" ]]; then
            rlRun "modprobe prng" 0 "Load kernel module for /dev/prandom device"
            rlRun "[[ \"$DEV_PRANDOM_PERMS\" == \"$(stat -c '%a' /dev/prandom)\" ]]" 0 \
                  "/dev/prandom should have 0644"
            rlLogInfo "$(stat -c '%a %n' /dev/prandom)"
        else
            rlLogInfo "Not on s390x, skipping...";
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
